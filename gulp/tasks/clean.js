var gulp = require("gulp");
var config = require("../config.js");
var clean = require("gulp-clean");

gulp.task("clean", function () {
  return gulp.src(config.dest.root, {read: false})
  .pipe(clean());
});
