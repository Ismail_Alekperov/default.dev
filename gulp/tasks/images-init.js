var gulp = require("gulp");
const config = require("../config.js");

gulp.task("images:init", function () {
  // Copy images
  gulp.src(config.src.img + "*.*")
  .pipe(gulp.dest(config.dest.img));

  // Copy user images
  gulp.src(config.src.images + "*.*")
  .pipe(gulp.dest(config.dest.images));
});