var gulp = require("gulp");
const config = require("../config.js");

gulp.task("fonts:init", function () {
  gulp.src(config.src.fonts + "**/*.*")
  .pipe(gulp.dest(config.dest.fonts));
});