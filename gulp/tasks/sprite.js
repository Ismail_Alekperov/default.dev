var gulp = require("gulp");
const config = require("../config.js");
var spritesmith = require("gulp.spritesmith");

// Icon sprite
gulp.task("sprite", function () {
  var spriteData = gulp.src(config.src.iconsPNG + "*.png")
  .pipe(spritesmith({
    imgName: "iconostas.png",
    imgPath: "../img/iconostas.png",
    padding: 5,
    cssName: "iconostas.less"
  }));
  spriteData.img.pipe(gulp.dest(config.src.img));
  spriteData.css.pipe(gulp.dest(config.src.css));
});