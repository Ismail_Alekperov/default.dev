var gulp = require("gulp");
const config = require("../config.js");
var spritesmith = require("gulp.spritesmith");

// Icon sprite
gulp.task("sprite:retina", function () {
  var spriteData = gulp.src(config.src.iconsPNG + "*.png")
  .pipe(spritesmith({
    imgName: "iconostas.png",
    imgPath: "../img/iconostas.png",
    retinaImgName: "iconostas@2x.png",
    retinaImgPath: "../img/iconostas@2x.png",
    retinaSrcFilter: config.src.iconsPNG + "*@2x.png",
    padding: 5,
    cssName: "iconostas.less"
  }));
  spriteData.img.pipe(gulp.dest(config.src.img));
  spriteData.css.pipe(gulp.dest(config.src.css));
});