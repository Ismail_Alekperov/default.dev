var notifier = require("node-notifier");
var notify = require("gulp-notify");
var colors = require("ansi-colors");
var log = require("fancy-log");

module.exports = function (title) {
  return function (error) {
    log([
      colors.bold(colors.bgred(" " + title + " ")),
      "",
      colors.magenta("plugin: ") + error.plugin,
      "",
      colors.magenta("message: ") + error.message,
      ""
    ].join("\n"));
    notifier.notify({
      "title": title,
      "message": "Look in terminal"
    });
    this.emit("end");
  };
};