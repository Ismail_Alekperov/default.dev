var gulp = require("gulp");
var path = require("../config.js").path;
var nunjucksRender = require("gulp-nunjucks-render");
var prettify = require("gulp-html-prettify");
var notifier = require("node-notifier");
var uncache = require("gulp-uncache");
var connect = require("gulp-connect");
var data = require("gulp-data");
var fs = require("fs");
var gulpif = require("gulp-if");
var config = require("../config.js");
var watch = require("gulp-watch");
var plumber = require("gulp-plumber");
var errorHandler = require("./error-handler");

const templates = [config.src.templates];
const environment = function(env) {
  config.build ? env.addGlobal("NODE_ENV", "build") : env.addGlobal("NODE_ENV", config.env);
};

gulp.task("nunjucks", function () {
  return gulp.src(config.src.templates + "*.+(twig)")
  .pipe(plumber({errorHandler: errorHandler("Error in [nunjuck] task")}))
  .pipe(data(function (file) {
    return JSON.parse(fs.readFileSync(config.src.data + "data.json"));
  }))
  .pipe(nunjucksRender({
    path: templates,
    manageEnv: environment
  }))
  .pipe(uncache())
  .pipe(prettify({indent_char: "\t", indent_size: 1}))
  .pipe(gulp.dest(config.dest.root))
  .pipe(connect.reload());
});

gulp.task("data:watch", function () {
  watch(config.src.data + "data.json", function (event, cb) {
    gulp.start("nunjucks");
  });
});

gulp.task("nunjucks:watch", function () {
  watch(config.src.templates + "**/*.twig", function (event, cb) {
    gulp.start("nunjucks");
  });
});