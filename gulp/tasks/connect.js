var gulp = require("gulp");
var connect = require("gulp-connect");
const config = require("../config.js");

// Server
gulp.task("connect", function () {
  connect.server({
    host: "0.0.0.0",
    root: config.dest.root,
    port: 1357,
    livereload: true
  });
});