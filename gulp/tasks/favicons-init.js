var gulp = require("gulp");
const config = require("../config.js");

gulp.task("favicons:init", function () {
  gulp.src(config.src.favicons + "*.*")
  .pipe(gulp.dest(config.dest.root));
});