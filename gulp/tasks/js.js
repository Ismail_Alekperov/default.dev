var gulp = require("gulp");
var connect = require("gulp-connect");
var watch = require("gulp-watch");
var fileinclude = require("gulp-file-include");
var include = require("gulp-include");
var uglify = require("gulp-uglify");
var notifier = require("node-notifier");
var notify = require("gulp-notify");
var gulpif = require("gulp-if");
var config = require("../config.js");
var rename = require("gulp-rename");
var plumber = require("gulp-plumber");
var errorHandler = require("./error-handler");
var sourcemaps = require("gulp-sourcemaps");

gulp.task("js", function () {
  gulp.src(config.src.scripts + "app.js")
  .pipe(plumber({errorHandler: errorHandler("Error in [js] task")}))
  .pipe(gulpif(!config.build, sourcemaps.init()))
  .pipe(include())
  .pipe(gulpif(config.build, uglify()))
  .pipe(gulpif(config.build, rename({suffix: ".min"})))
  .pipe(notify({
    "title": "JS compilation",
    "message": "Everything is fine!!"
  }))
  .pipe(gulpif(!config.build, sourcemaps.write("")))
  .pipe(gulp.dest(config.dest.scripts));
});

gulp.task("js:watch", function () {
  watch(config.src.scripts + "**/*.js", function (event, cb) {
    gulp.start("js");
    gulp.start("nunjucks");
  });
});