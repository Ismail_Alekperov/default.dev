var gulp = require("gulp");
var sourcemaps = require("gulp-sourcemaps");
var less = require("gulp-less");
var notifier = require("node-notifier");
var notify = require("gulp-notify");
var postcss = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var postcssSVG = require("postcss-svg");
var csso = require("postcss-csso");
var connect = require("gulp-connect");
var watch = require("gulp-watch");
var rename = require("gulp-rename");
var plumber = require("gulp-plumber");
var gulpif = require("gulp-if");
var config = require("../config.js");
var errorHandler = require("./error-handler");

// Less
gulp.task("less", function () {
  gulp.src(config.src.css + "main.less")
  .pipe(plumber({errorHandler: errorHandler("Error in [less] task")}))
  .pipe(gulpif(!config.build, sourcemaps.init()))
  .pipe(less())
  .pipe(gulpif(
    config.build,
    postcss([
      autoprefixer({
        browsers: ["last 2 versions"]
      }),
      csso()
    ])
  ))
  .pipe(gulpif(config.build, rename({suffix: ".min"})))
  .pipe(notify("Everything is fine!"))
  .pipe(gulpif(!config.build, sourcemaps.write("")))
  .pipe(gulp.dest(config.dest.css));
});

// Watch for less
gulp.task("less:watch", function () {
  watch(config.src.css + "**/*.less", function (event, cb) {
    gulp.start("less");
    gulp.start("nunjucks");
  });
});