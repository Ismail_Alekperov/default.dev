var gulp = require("gulp");
var buffer = require("vinyl-buffer");
var merge = require("merge-stream");
var debug = require("gulp-debug");
const runSequence = require("run-sequence");
var gulpif = require("gulp-if");
const config = require("../config.js");

gulp.task("default", function (callback) {
  runSequence(
    "clean",
    [
      "favicons:init",
      "fonts:init",
      "images:init",
      "sprite:svg",
      "sprite:svg:watch",
      "nunjucks",
      "nunjucks:watch",
      "data:watch",
      "less",
      "less:watch",
      "js",
      "js:watch",
      "connect",
    ],
    callback
  );
  config.setEnv("development");
  config.logEnv();
});

gulp.task("build", function (callback) {
  runSequence(
    "clean",
    [
      "favicons:init",
      "fonts:init",
      "images:init",
      "sprite:svg",
      "nunjucks",
      "less",
      "js",
    ],
    callback
  );
  config.setEnv("build");
  config.logEnv();
});