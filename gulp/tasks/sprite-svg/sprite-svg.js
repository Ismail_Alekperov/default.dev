var gulp = require("gulp");
var plumber = require("gulp-plumber");
var svgmin = require("gulp-svgmin");
var svgStore = require("gulp-svgstore");
var rename = require("gulp-rename");
var debug = require("gulp-debug");
var colors = require("ansi-colors");
var cheerio = require("gulp-cheerio");
var through2 = require("through2");
var watch = require("gulp-watch");
var consolidate = require("gulp-consolidate");
var errorHandler = require(".././error-handler");
var config = require("../../config.js");

gulp.task("sprite:svg", function () {
  return gulp
  .src(config.src.iconsSVG + "*.svg")
  .pipe(plumber({errorHandler: errorHandler("Error in [sprite:svg] task")}))
  .pipe(svgmin({
    js2svg: {
      pretty: true
    },
    plugins: [{
      removeDesc: true
    }, {
      cleanupIDs: true
    }, {
      mergePaths: false
    }]
  }))
  .pipe(rename({prefix: "icon-"}))
  .pipe(svgStore({inlineSvg: false}))
  .pipe(through2.obj(function (file, encoding, cb) {
    var $ = file.cheerio;
    var data = $("svg > symbol").map(function () {
      var $this = $(this);
      var size = $this.attr("viewBox").split(" ").splice(2);
      var name = $this.attr("id");
      var ratio = size[0] / size[1]; // symbol width / symbol height
      var fill = $this.find("[fill]:not([fill='currentColor'])").attr("fill");
      var stroke = $this.find("[stroke]").attr("stroke");
      return {
        name: name,
        ratio: +ratio.toFixed(2),
        fill: fill || "initial",
        stroke: stroke || "initial"
      };
    }).get();
    this.push(file);
    gulp.src(__dirname + "/sprite-svg.txt")
    .pipe(consolidate("lodash", {
      symbols: data
    }))
    .pipe(rename({extname: ".less"}))
    .pipe(gulp.dest(config.src.css));
    gulp.src(__dirname + "/sprite.html")
    .pipe(consolidate("lodash", {
      symbols: data
    }))
    // .pipe(debug({title: colors.bgred(" DEBUG2: ")}))
    .pipe(gulp.dest(config.src + "dddd/"));
    cb();
  }))
  .pipe(cheerio({
    run: function ($, file) {
      $("[fill]:not([fill='currentColor'])").removeAttr("fill");
      $("[stroke]").removeAttr("stroke");
    },
    parserOptions: {xmlMode: true}
  }))
  .pipe(rename({basename: "sprite"}))
  .pipe(gulp.dest(config.dest.img));
});

gulp.task("sprite:svg:watch", function () {
  watch(config.src.iconsSVG + "*.svg", function (event, cb) {
    gulp.start("sprite:svg");
    gulp.start("nunjucks");
  });
});