const log = require("fancy-log");
const colors = require("ansi-colors");
const minimist = require('minimist');
const argv = minimist(process.argv.slice(2));

const config = {
  src: {
    root     : "src",
    templates: "src/templates/",
    data     : "src/data/",
    css      : "src/css/",
    scripts  : "src/scripts/",
    images   : "src/images/",
    img      : "src/img/opt/",
    favicons : "src/favicons/",
    iconsSVG : "src/img/svg-icons/",
    iconsPNG : "src/img/icons/",
    fonts    : "src/fonts/"
  },
  dest: {
    root   : "build",
    css    : "build/css/",
    scripts: "build/scripts/",
    images : "build/images/",
    img    : "build/img/",
    fonts  : "build/fonts/"
  },

  env: "development",
  build: argv.build || false,

  setEnv: function(env) {
    if (typeof env !== "string") return;
    this.env = env;
    this.build = env === "build";
    process.env.NODE_ENV = env;
  },

  logEnv: function() {
    log(
      colors.green("Environment:"),
      colors.bold(colors.red("[" + process.env.NODE_ENV + "]"))
    );
  },
};

module.exports = config;
